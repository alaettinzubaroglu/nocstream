##########
# NoCStream.py
#   NoCStream: Determining the Optimal Number of Clusters on High Dimensional Evolving Data Streams
#
##########

from sklearn.metrics import silhouette_score
from sklearn.cluster import MeanShift
from sklearn.cluster import estimate_bandwidth
from sklearn.cluster import KMeans


#####
def predict_k(X, embedding, bandwidth_list, estimate_bw):
    max_sil = 0
    k_maxsil = 0
    bw_maxsil = 0

    for bw in bandwidth_list:
        meanshift = MeanShift(bandwidth=bw, n_jobs=-1).fit(embedding)
        cluster_cnt = len(set(meanshift.labels_))
        if cluster_cnt > 1:
            ss = silhouette_score(X, meanshift.labels_)
        else:
            ss = 0

        if ss > max_sil:
            max_sil = ss
            k_maxsil = cluster_cnt
            bw_maxsil = bw


        if __debug__:
            print('bw : {}'.format(bw))
            print('Embedded : k:{} silhouette:{}'.format(cluster_cnt, ss))
            print('----')

        if ss == 0:
            break;

    if estimate_bw:
	    estimated_bw = estimate_bandwidth(embedding)
	    meanshift = MeanShift(bandwidth=estimated_bw, n_jobs=-1).fit(embedding)
	    cluster_cnt = len(set(meanshift.labels_))
	    if cluster_cnt > 1:
	        ss = silhouette_score(X, meanshift.labels_)
	    else:
	        ss = 0   

	    if ss > max_sil:
	        max_sil = ss
	        k_maxsil = cluster_cnt
	        bw_maxsil = estimated_bw

    return k_maxsil
##### end of predict_k

#####
def predict_k_change(X, embedding, cluster_cnt, range_delta):
    max_sil = 0
    k_maxsil = 0

    k_alternatives = [x for x in range(cluster_cnt-range_delta, cluster_cnt+range_delta+1) if x > 1]

    for candidate_k in k_alternatives:
        kmeans = KMeans(n_clusters=candidate_k, random_state=0).fit(embedding)
        klabels = [x for x in kmeans.labels_]
        siluet = silhouette_score(X, klabels)
        if siluet > max_sil:
            max_sil = siluet
            k_maxsil = candidate_k

        if __debug__:
            print('predict_k_change is trying [k:{}], [silhouette score:{}]'.format(candidate_k, siluet))

    return k_maxsil
##### end of predict_k_change